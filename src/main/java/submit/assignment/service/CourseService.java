package submit.assignment.service;


import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import submit.assignment.domain.Course;
import submit.assignment.domain.User;
import submit.assignment.repository.CourseRepo;
import submit.assignment.repository.UserRepo;
import submit.assignment.security.SecurityUtils;
import submit.assignment.service.dto.*;
import submit.assignment.service.exceptions.CourseNotFoundException;
import submit.assignment.service.exceptions.EmailDoesNotExistException;
import submit.assignment.service.mapper.CourseMapper;
import submit.assignment.service.mapper.UserMapper;
import submit.assignment.service.util.RandomUtil;
import submit.assignment.service.vm.CourseVM;

import java.nio.file.attribute.UserPrincipalNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class CourseService {
    private final CourseRepo courseRepo;

    private final UserRepo userRepo;

    private final CourseMapper courseMapper;

    private final UserMapper userMapper;



    public CourseService(CourseRepo courseRepo, UserRepo userRepo, CourseMapper courseMapper, UserMapper userMapper) {
        this.courseRepo = courseRepo;

        this.userRepo = userRepo;
        this.courseMapper = courseMapper;
        this.userMapper = userMapper;
    }

    //create course
    public Course createCourse(CourseVM courseVM) throws EmailDoesNotExistException {
        Optional<User> userOptional = userRepo.findByEmailIgnoreCase(courseVM.getInstructor());
        if(userOptional.isEmpty()){
            throw new EmailDoesNotExistException("The provided email does not exist");

        }

        else {
            Course course = new Course();
            course.setCourseCode(courseVM.getCourseCode());
            course.setCourseName(courseVM.getCourseName());
            course.setCourseDescription(courseVM.getCourseDescription());
            String link = "http://localhost:8080/join-course?key="+RandomUtil.generateCourseEnrollmentKey();
            course.setEnrollmentLink(link);
            course.setEnrollmentKey(RandomUtil.generateEnrollmentKey());
            course.setCourseInstructor(courseVM.getInstructor());
            courseRepo.save(course);

            log.info("Adding course id to the user : {}", course.getCourseInstructor() );

            addCourseIdToUser(course.getCourseInstructor(), course.getEnrollmentKey());

            return course;

        }


    }

    //add course Id to the user's table
    public User addCourseIdToUser(String email, Long courseKey){
        //find user
        Optional<User> userOptional = userRepo.findByEmailIgnoreCase(email);
        if (userOptional.isEmpty()){
            log.info("User not found");
        }
        User user = userOptional.get();
        //get the course
        Optional<Course> courseOptional = courseRepo.findByEnrollmentKey(courseKey);
        if (courseOptional.isEmpty()){
            log.info("Course to be added is not found");
        }
        Course course = courseOptional.get();


        UserDTO userDTO = new UserDTO();
        userDTO = userMapper.toDTO(user);

        List<String> courseIdsList = userDTO.getCourseIds();

        courseIdsList.add(String.valueOf(course.getId()));

        userDTO.setCourseIds(courseIdsList);

        log.info("Updated the users course ids with : {} elements", courseIdsList.size());

        log.info("Updated the course Ids for user: {}", course.getCourseInstructor());
        return userRepo.save(userMapper.toEntity(userDTO));


    }



    //student enroll to a course
    public CourseDTO enrollCourse(EnrollCourseDTO enrollCourseDTO) throws CourseNotFoundException {
        //check the student's email if it exists
        Optional<User> userOptional = userRepo.findByEmailIgnoreCase(enrollCourseDTO.getEmail());

        if (userOptional.isPresent()){

            //fetch course
            CourseDTO courseDTO = new CourseDTO();
            Optional<Course> courseOptional = courseRepo.findByEnrollmentKey(enrollCourseDTO.getEnrollmentKey());
            if(courseOptional.isPresent()){
                Course course = courseOptional.get();
                log.info("Retrieved course {} for student to enroll in", course.getId());

                courseDTO = courseMapper.toDTO(course);
                log.info("Mapped the course to its DTO in readiness for a student to enroll");
                List<String> enrolledStudentsList = new ArrayList<>();
                 if(courseDTO.getEnrolledStudents() != null){
                     enrolledStudentsList = courseDTO.getEnrolledStudents();
                 }
                 log.info("Initialized enrolled students  with {} elements", enrolledStudentsList.size());

                 log.info("Course DTO : {}", courseDTO);
                 enrolledStudentsList.add(enrollCourseDTO.getEmail());
                 log.info("Enrolled Students : {}", enrolledStudentsList);

                 log.info("updated the enrolled student list");

                courseDTO.setEnrolledStudents(enrolledStudentsList);
                courseDTO.setEnrollmentKey(course.getEnrollmentKey());
                courseDTO.setEnrollmentLink(course.getEnrollmentLink());
               

                log.info("Course DTO: {}", courseDTO);

                courseRepo.save(courseMapper.toEntity(courseDTO));

                log.info("Saved the updated course");

                addCourseIdToUser(enrollCourseDTO.getEmail(), enrollCourseDTO.getEnrollmentKey());


            }
            return courseDTO;
        }
        throw new CourseNotFoundException("Course you are enrolling to is not found");


    }

    //fetch courses
    public List<Course> fetchCourses(String email) {
        List<Course> courseList = new ArrayList<>();

        Optional<User> userOptional = userRepo.findByEmailIgnoreCase(email);
        if (userOptional.isPresent()){
            User user = userOptional.get();
            UserDTO userDTO = userMapper.toDTO(user);
            List<String> courses = userDTO.getCourseIds();
            for(int i = 0; i<courses.size(); i++){
                Long courseId = Long.valueOf(courses.get(i));
                Optional<Course> courseOptional = courseRepo.findById(courseId);

                if (courseOptional.isPresent()){
                    Course course = courseOptional.get();
                    courseList.add(course);
                }
            }
        }

        log.info("Found {} course(s) for the user", courseList.size());
        return courseList;


    }
}
