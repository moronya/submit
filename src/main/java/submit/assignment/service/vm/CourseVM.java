package submit.assignment.service.vm;

import lombok.Data;

import javax.persistence.Column;

@Data
public class CourseVM {
    private String courseCode;

    private String courseName;

    private String courseDescription;

    private String instructor;
}
