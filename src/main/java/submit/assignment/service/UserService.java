package submit.assignment.service;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.extern.slf4j.Slf4j;
import okhttp3.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import submit.assignment.configurations.ApplicationProperties;
import submit.assignment.domain.User;
import submit.assignment.repository.UserRepo;
import submit.assignment.security.SecurityUtils;
import submit.assignment.service.dto.*;
import submit.assignment.service.exceptions.EmailDoesNotExistException;
import submit.assignment.service.exceptions.EmailExistsException;
import submit.assignment.service.exceptions.InvalidResetKeyException;
import submit.assignment.service.util.HttpUtil;
import submit.assignment.service.util.RandomUtil;
import submit.assignment.service.util.TemplateUtil;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
@Slf4j
public class UserService {
    private final  UserRepo userRepo;
    private final ApplicationProperties applicationProperties;

    BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
    private final ObjectMapper objectMapper = new ObjectMapper().configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);

    public UserService(UserRepo userRepo, ApplicationProperties applicationProperties) {
        this.userRepo = userRepo;
        this.applicationProperties = applicationProperties;

    }
    //First user registration
    public User register(InitialRegistrationDTO initialRegistrationDTO) throws IOException, EmailExistsException {

        //check whether the email has been taken already
        Optional<User> userOptional = userRepo.findByEmailIgnoreCase(initialRegistrationDTO.getEmail());
        log.info("Checking if the email {} already exists", initialRegistrationDTO.getEmail());
        if (userOptional.isPresent()){
            throw new EmailExistsException("The email has been taken");
        }
        User user = new User();
        user.setEmail(initialRegistrationDTO.getEmail());
        user.setRole(initialRegistrationDTO.getRole());
        user.setRegistrationDate(LocalDateTime.now());
        user.setResetKey(RandomUtil.generateResetkey());

        //send mail to the user using notify
        Long activation = user.getResetKey();

        String body = TemplateUtil.activateAccountEmail(activation);

        String subject = "Account Activation - Submit";

        EmailDTO emailDTO = new EmailDTO(user.getEmail(), subject, body, true, false);

        log.info("Email DTO {}", emailDTO);

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("apiKey", applicationProperties.getApiKey());
        log.info("apiKey : {} ", headerMap);

        String sendMailUrl = applicationProperties.getNotify() + "send-email";

        HttpUtil.post(sendMailUrl, objectMapper.writeValueAsString(emailDTO), headerMap, MediaType.get("application/json; charset=utf-8"));
        log.info("Account activation Email has been sent to : {} ", user.getEmail());

        return userRepo.save(user);
    }

    //completing User registration
    public User completeUserRegistration(AccountInfoDTO accountInfoDTO) throws InvalidResetKeyException {
        //check whether the user exists
        Optional<User> userOptional = userRepo.findByResetKey(accountInfoDTO.getResetKey());
        log.info("Checking the existence of the reset Key : {}", accountInfoDTO.getResetKey());
        if (userOptional.isEmpty()){
            throw new InvalidResetKeyException("The reset key entered does not exist");
        }
        //update the user

        User user = userOptional.get();

        user.setPassword(bCryptPasswordEncoder.encode(accountInfoDTO.getPassword()));
        user.setActivated(true);
        user.setResetKey(null);

        return userRepo.save(user);

    }

    @Transactional(readOnly = true)
 public Optional<User> getUserWithRolesByLogin(String login){
        return userRepo.findOneWithRoleByEmail(login);
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithAuthorities() {
        return SecurityUtils.getCurrentUserLogin().flatMap(userRepo::findOneWithRoleByEmail);
    }

    // request reset password
    public User requestPasswordReset(RequestPasswordReset requestPasswordReset) throws EmailDoesNotExistException, IOException {
        //check if the email supplied exists

        Optional<User> userOptional = userRepo.findByEmailIgnoreCase(requestPasswordReset.getEmail());

        if(userOptional.isEmpty()){
            log.error("The user email supplied does not exist : {}", requestPasswordReset.getEmail());
            throw new EmailDoesNotExistException("The email does not exist");
        }
        else{
            //get the user
            User user = userOptional.get();
            user.setResetKey(RandomUtil.generateResetkey());


            //send mail to the user using notify
            Long activation = user.getResetKey();

            String body = TemplateUtil.resetPassword(activation);

            String subject = "Reset Password - Submit";

            EmailDTO emailDTO = new EmailDTO(user.getEmail(), subject, body, true, false);

            log.info("Email DTO {}", emailDTO);

            Map<String, String> headerMap = new HashMap<>();
            headerMap.put("apiKey", applicationProperties.getApiKey());
            log.info("apiKey : {} ", headerMap);

            String sendMailUrl = applicationProperties.getNotify() + "send-email";

            HttpUtil.post(sendMailUrl, objectMapper.writeValueAsString(emailDTO), headerMap, MediaType.get("application/json; charset=utf-8"));
            log.info("An email to reset password has been sent to : {} ", user.getEmail());

            return userRepo.save(user);
        }
    }

    //change password
    public User resetPassword(ResetPasswordDTO resetPasswordDTO) throws InvalidResetKeyException {
        //find user by reset key
        Optional<User> userOptional = userRepo.findByResetKey(resetPasswordDTO.getResetKey());

        if (userOptional.isEmpty()){
            log.error("The specified reset Key found does not exist");
            throw new InvalidResetKeyException("Invalid reset key");
        }
        else{
            //get the user
            User user = userOptional.get();
            log.info("Retrieved user {}, ready to reset their password", user.getEmail());
            user.setPassword(bCryptPasswordEncoder.encode(resetPasswordDTO.getNewPassword()));
            user.setResetKey(null);
            return userRepo.save(user);
        }
    }





}
