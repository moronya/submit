package submit.assignment.service;


import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import submit.assignment.configurations.ApplicationProperties;
import submit.assignment.domain.Assignment;
import submit.assignment.domain.Submission;
import submit.assignment.domain.User;
import submit.assignment.repository.AssignmentRepo;
import submit.assignment.repository.SubmissionRepo;
import submit.assignment.repository.UserRepo;
import submit.assignment.service.dto.SubmissionDTO;
import submit.assignment.service.dto.SubmissionDetailDTO;
import submit.assignment.service.exceptions.AssignmentExceptions;
import submit.assignment.service.exceptions.EmailDoesNotExistException;
import submit.assignment.service.mapper.SubmissionDetailMapper;
import submit.assignment.service.util.StringUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class SubmissionService {
    private final SubmissionRepo submissionRepo;
    private final UserRepo userRepo;

    private final AssignmentRepo assignmentRepo;

    private final ApplicationProperties applicationProperties;

    private final SubmissionDetailMapper submissionDetailMapper;

    public static String DELIMETER =",";

    public SubmissionService(SubmissionRepo submissionRepo, UserRepo userRepo, AssignmentRepo assignmentRepo, ApplicationProperties applicationProperties, SubmissionDetailMapper submissionDetailMapper) {
        this.submissionRepo = submissionRepo;
        this.userRepo = userRepo;
        this.assignmentRepo = assignmentRepo;
        this.applicationProperties = applicationProperties;
        this.submissionDetailMapper = submissionDetailMapper;
    }

    public Submission createSubmission(SubmissionDTO submissionDTO, List<MultipartFile> multipartFileList) throws EmailDoesNotExistException, AssignmentExceptions, IOException {
        //retrieve student's admission number and Name using their email address.
        Optional<User> userOptional = userRepo.findByEmailIgnoreCase(submissionDTO.getEmail());

        if(userOptional.isEmpty()){
            throw new EmailDoesNotExistException("User does not exist");
        }
        else{

//            User user = userOptional.get();
            Submission submission = new Submission();
            submission.setAdmissionNumber(submissionDTO.getAdmissionNumber());
            submission.setStudentName(submissionDTO.getStudentName());
            submission.setAssignmentKey(submissionDTO.getAssignmentKey());
            submission.setEmail(submissionDTO.getEmail());
            submission.setSubmittedAt(LocalDateTime.now());

            //get assignment folder to populate the files based on the assignment Key
            Optional<Assignment> optionalAssignment = assignmentRepo.findByAssignmentKey(submissionDTO.getAssignmentKey());

            if (optionalAssignment.isEmpty()){
                throw new AssignmentExceptions("Assignment key does not exist");
            }

            Assignment assignment = optionalAssignment.get();
            List<String>  uploadFilesList = uploadFile(multipartFileList, assignment.getAssignmentKey());
            submission.setFileUrls(StringUtil.combineBy(uploadFilesList, DELIMETER));
            return  submissionRepo.save(submission);
        }
    }

  public List<String> uploadFile(List<MultipartFile> multipartFileList, String assignmentKey) throws IOException {
        // file path
      Optional<Assignment> assignmentOptional = assignmentRepo.findByAssignmentKey(assignmentKey);
      Assignment assignment = assignmentOptional.get();

      //get assignment file folder where all submissions will be uploaded
      String folderName = assignment.getFolderName();

      String mediaFolder = applicationProperties.getFolderName();

      String mediaStore = mediaFolder +"/"+folderName;

      Path path = Paths.get(mediaStore);

      //create path if it does not exist

      if (!Files.exists(path)) {
          Files.createDirectories(path);
      }

      List<String> fileNames = new ArrayList<>();
      for (MultipartFile multipartFile:multipartFileList){
          File fileToWrite = new File(mediaStore, multipartFile.getOriginalFilename());

          fileNames.add(multipartFile.getOriginalFilename());

          FileOutputStream fileOutputStream = new FileOutputStream(fileToWrite);
          fileOutputStream.write(multipartFile.getBytes());
      }

      return fileNames;
  }

  //get submission all submission by assignmentKey
    public List<SubmissionDetailDTO> getAllSubmissions(String assignmentKey){
        List<SubmissionDetailDTO> submissionList = new ArrayList<>();

        Iterable<Submission> submissionIterable = submissionRepo.findByAssignmentKey(assignmentKey);


        for (Submission submission:submissionIterable){
            //change the submission to submissionDetailDTO
            SubmissionDetailDTO submissionDetailDTO = submissionDetailMapper.toDTO(submission);
            submissionList.add(submissionDetailDTO);
        }

        log.info("Found : {} submissions for assignment : {}", submissionList.size(), assignmentKey );

        return submissionList;
    }
}
