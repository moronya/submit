package submit.assignment.service.exceptions;

public class InvalidResetKeyException extends Exception{
    public InvalidResetKeyException(String message){
        super(message);
    }
}
