package submit.assignment.service.exceptions;

public class EmailDoesNotExistException extends Exception{
    public EmailDoesNotExistException(){

    }
    public EmailDoesNotExistException(String message){
        super(message);
    }

}
