package submit.assignment.service.exceptions;

public class AssignmentExceptions extends Exception{
    public AssignmentExceptions (String message){
        super (message);
    }
}
