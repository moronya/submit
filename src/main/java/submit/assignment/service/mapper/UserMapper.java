package submit.assignment.service.mapper;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import submit.assignment.domain.Role;
import submit.assignment.domain.User;
import submit.assignment.service.dto.CourseIdsDTO;
import submit.assignment.service.dto.UserDTO;
import submit.assignment.service.util.StringUtil;

import java.time.LocalDateTime;
import java.util.List;

@Service
@Slf4j
public class UserMapper {
    public static String DELIMETER =",";

    private final ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.
            ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true).configure(JsonParser.
            Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
    public User toEntity(UserDTO userDTO){
        if (userDTO == null){
            return null;
        }
        User user = new User();

        user.setId(userDTO.getId());
        user.setEmail(userDTO.getEmail());
        user.setPassword(userDTO.getPassword());
        user.setRole(userDTO.getRole());
        user.setResetKey(userDTO.getResetKey());
        user.setRegistrationDate(userDTO.getRegistrationDate());
        user.setUpdatedOn(userDTO.getUpdatedOn());
        user.setActivated(userDTO.isActivated());

        //check if course Ids exist
        if(userDTO.getCourseIds() == null || userDTO.getCourseIds().isEmpty()){
            log.info("Course Ids are not present, setting up...");
            user.setCourseIds(null);
        }
        else{
            log.info("There are existing course ids, trying to map");
            user.setCourseIds(StringUtil.combineBy(userDTO.getCourseIds(), DELIMETER));
        }

        return user;
    }

    public UserDTO toDTO(User user){
        if (user == null){
            return null;
        }
        UserDTO userDTO = new UserDTO();

        userDTO.setId(user.getId());
        userDTO.setEmail(user.getEmail());
        userDTO.setPassword(user.getPassword());
        userDTO.setRole(user.getRole());
        userDTO.setResetKey(user.getResetKey());
        userDTO.setRegistrationDate(user.getRegistrationDate());
        userDTO.setUpdatedOn(userDTO.getUpdatedOn());
        userDTO.setActivated(user.isActivated());

        userDTO.setCourseIds(StringUtil.splitString(user.getCourseIds(), DELIMETER));

        return userDTO;

    }
}
