package submit.assignment.service.mapper;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import submit.assignment.domain.Assignment;
import submit.assignment.service.dto.AssignmentDTO;
import submit.assignment.service.util.StringUtil;


@Service
@Slf4j
public class AssignmentMapper {
   public static String DELIMITER =",";

    private final ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature
            .ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true).configure(JsonParser.Feature
            .ALLOW_UNQUOTED_FIELD_NAMES, true);

    public Assignment toEntity(AssignmentDTO assignmentDTO){
        if (assignmentDTO == null){
            return null;
        }
        Assignment assignment = new Assignment();

        assignment.setAssignmentKey(assignmentDTO.getAssignmentKey());
        assignment.setFolderName(assignmentDTO.getFolderName());
        assignment.setId(assignment.getId());
        assignment.setCourseId(assignmentDTO.getCourseId());
        assignment.setTitle(assignmentDTO.getTitle());
        assignment.setInstructions(assignmentDTO.getInstructions());
        assignment.setCreatedOn(assignmentDTO.getCreatedOn());
        assignment.setDeadline(assignmentDTO.getDeadline());
        assignment.setAssignmentStatus(assignmentDTO.getAssignmentStatus());


//        //setting up file urls
        if (assignmentDTO.getFileUrls() == null || assignmentDTO.getFileUrls().isEmpty()){
            log.debug("File urls is empty, setting up ....");
            assignment.setFileUrls(null);
        }
        else{
            log.info("There are existing files, trying to map");
            assignment.setFileUrls(StringUtil.combineBy(assignmentDTO.getFileUrls(), DELIMITER));
        }
        return assignment;
    }

    public AssignmentDTO toDTO(Assignment assignment){
        if (assignment == null){
            return null;
        }
        AssignmentDTO assignmentDTO = new AssignmentDTO();

        assignmentDTO.setFolderName(assignment.getFolderName());
        assignmentDTO.setAssignmentKey(assignment.getAssignmentKey());
        assignmentDTO.setId(assignment.getId());
        assignmentDTO.setCourseId(assignment.getCourseId());
        assignmentDTO.setTitle(assignment.getTitle());
        assignmentDTO.setInstructions(assignment.getInstructions());
        assignmentDTO.setDeadline(assignment.getDeadline());
        assignmentDTO.setCreatedOn(assignment.getCreatedOn());
        assignmentDTO.setAssignmentStatus(assignment.getAssignmentStatus());

        assignmentDTO.setFileUrls(StringUtil.splitString(assignment.getFileUrls(), DELIMITER));


        return assignmentDTO;


    }
}
