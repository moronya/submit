package submit.assignment.service.mapper;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import submit.assignment.domain.Submission;
import submit.assignment.service.dto.SubmissionDetailDTO;
import submit.assignment.service.util.StringUtil;

@Service
@Slf4j
public class SubmissionDetailMapper {
    public static String DELIMITER =",";

    private final ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature
            .ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true).configure(JsonParser.Feature
            .ALLOW_UNQUOTED_FIELD_NAMES, true);

    public Submission toEntity(SubmissionDetailDTO submissionDetailDTO){
        if (submissionDetailDTO == null){
            return null;
        }
        Submission submission = new Submission();

        submission.setAssignmentKey(submissionDetailDTO.getAssignmentKey());
        submission.setId(submissionDetailDTO.getId());
        submission.setAdmissionNumber(submissionDetailDTO.getAdmissionNumber());
        submission.setStudentName(submissionDetailDTO.getStudentName());
        submission.setSubmittedAt(submissionDetailDTO.getSubmittedAt());

        submission.setFileUrls(StringUtil.combineBy(submissionDetailDTO.getFileUrls(), DELIMITER));

        return submission;
    }

    public SubmissionDetailDTO toDTO(Submission submission){
        if (submission == null){
            return null;
        }
        SubmissionDetailDTO submissionDetailDTO = new SubmissionDetailDTO();

        submissionDetailDTO.setAssignmentKey(submission.getAssignmentKey());
        submissionDetailDTO.setId(submission.getId());
        submissionDetailDTO.setAdmissionNumber(submission.getAdmissionNumber());
        submissionDetailDTO.setStudentName(submission.getStudentName());
        submissionDetailDTO.setSubmittedAt(submission.getSubmittedAt());

        submissionDetailDTO.setFileUrls(StringUtil.splitString(submission.getFileUrls(), DELIMITER));

        return submissionDetailDTO;
    }

}
