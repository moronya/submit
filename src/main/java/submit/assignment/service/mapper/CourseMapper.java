package submit.assignment.service.mapper;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.type.TypeReference;
import org.springframework.stereotype.Service;
import submit.assignment.domain.Course;
import submit.assignment.service.dto.CourseDTO;
import submit.assignment.service.util.StringUtil;

import java.util.List;

@Service
public class CourseMapper {
    public static String DELIMETER =",";
    private final ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.
            ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true).configure(JsonParser.
            Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);


    public Course toEntity(CourseDTO courseDTO){
        if (courseDTO == null){
            return null;
        }
        Course course = new Course();
        course.setId(courseDTO.getId());
        course.setCourseCode(courseDTO.getCourseCode());
        course.setCourseInstructor(courseDTO.getCourseInstructor());
        course.setCourseName(courseDTO.getCourseName());
        course.setCourseDescription(courseDTO.getCourseDesciption());
        course.setEnrollmentLink(courseDTO.getEnrollmentLink());
        course.setEnrollmentKey(courseDTO.getEnrollmentKey());

        course.setEnrolledStudents(StringUtil.combineBy(courseDTO.getEnrolledStudents(), DELIMETER));


        return course;
    }

    public CourseDTO toDTO(Course course){
        CourseDTO courseDTO = new CourseDTO();
        courseDTO.setId(course.getId());
        courseDTO.setCourseInstructor(course.getCourseInstructor());
        courseDTO.setCourseCode(course.getCourseCode());
        courseDTO.setCourseName(course.getCourseName());
        courseDTO.setCourseDesciption(course.getCourseDescription());

        courseDTO.setEnrolledStudents(StringUtil.splitString(course.getEnrolledStudents(), DELIMETER));


        return courseDTO;
    }
}
