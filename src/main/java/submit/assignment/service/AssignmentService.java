package submit.assignment.service;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StreamUtils;
import org.springframework.web.multipart.MultipartFile;
import submit.assignment.configurations.ApplicationProperties;
import submit.assignment.domain.Assignment;
import submit.assignment.domain.AssignmentStatus;
import submit.assignment.domain.Course;

import submit.assignment.repository.AssignmentRepo;
import submit.assignment.repository.CourseRepo;
import submit.assignment.service.dto.AssignmentDTO;

import submit.assignment.service.exceptions.AssignmentExceptions;
import submit.assignment.service.exceptions.CourseNotFoundException;
import submit.assignment.service.mapper.AssignmentMapper;
import submit.assignment.service.util.RandomUtil;


import java.io.*;
import java.net.MalformedURLException;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.util.*;

@Service
@Slf4j
public class AssignmentService {
    private final AssignmentRepo assignmentRepo;
    private final CourseRepo courseRepo;
    private final ApplicationProperties applicationProperties;
    private final AssignmentMapper assignmentMapper;

    public AssignmentService(AssignmentRepo assignmentRepo, CourseRepo courseRepo, ApplicationProperties applicationProperties, AssignmentMapper assignmentMapper) {
        this.assignmentRepo = assignmentRepo;
        this.courseRepo = courseRepo;
        this.applicationProperties = applicationProperties;
        this.assignmentMapper = assignmentMapper;
    }

    //create an assignment
    public AssignmentDTO createAssignment(AssignmentDTO assignmentDTO, List<MultipartFile> multipartFiles) throws Exception {

        //check if the specified course Id exists

        Optional<Course> courseOptional = courseRepo.findByEnrollmentKey(assignmentDTO.getCourseId());
        if (courseOptional.isEmpty()) {
            log.info("Course with specified key not found");
            throw new CourseNotFoundException("Specified course does not exist");
        } else {

            String folderName = RandomUtil.generateFolderName();

            String mediaFolder = applicationProperties.getFolderName();


            Path path = Paths.get(mediaFolder, folderName);

            if (!Files.exists(path)){
                Files.createDirectories(path);
            }

            assignmentDTO.setCreatedOn(LocalDateTime.now());
            assignmentDTO.setAssignmentStatus(AssignmentStatus.OPEN);
            assignmentDTO.setFolderName(folderName);
            assignmentDTO.setAssignmentKey(RandomUtil.generateAssignmentKey());


            List<String> fileNames = new ArrayList<>();
            for (MultipartFile file : multipartFiles) {

                String mediaStore = mediaFolder + "/" + folderName;

                File fileToWrite = new File(mediaStore, file.getOriginalFilename());

                fileNames.add(file.getOriginalFilename());

                FileOutputStream fileOutputStream = new FileOutputStream(fileToWrite);
                fileOutputStream.write(file.getBytes());

                assignmentDTO.setFileUrls(fileNames);

            }

            log.info("Assignment DTO : {}", assignmentDTO);

            Assignment assignment = assignmentMapper.toEntity(assignmentDTO);
            assignment = assignmentRepo.save(assignment);

            log.info("Files uploaded successfully");

            return assignmentMapper.toDTO(assignment);
        }
    }

    public static String saveFile(String fileName, MultipartFile file) throws IOException {
        log.info("Request to upload file : {} ", file.getOriginalFilename());
        String folderName = "/home/amos/submit";

        Path path = Paths.get(folderName);

        //check if path exists and create one if it doesnt Exist
        if(!Files.exists(path)){
            try {
                Files.createDirectories(path);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        String fileKey = RandomStringUtils.randomAlphanumeric(20);
        Path filePath = path.resolve(fileKey +"-"+ fileName);

        InputStream inputStream = file.getInputStream();

        Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);

        return fileKey;
    }






    //fetch all assignments belonging to a course
    public List<Assignment> getAllAssignments(Long courseId) {
        List<Assignment> assignmentList = new ArrayList<>();

        Iterable<Assignment> assignmentIterable = assignmentRepo.findByCourseId(courseId);

        for (Assignment assignment : assignmentIterable) {
            assignmentList.add(assignment);
        }
        log.info("Populated {} assignments for course {} ", assignmentList.size(), courseId);

        return assignmentList;
    }

    public AssignmentDTO getOne(String key) throws AssignmentExceptions {
        Optional<Assignment> assignmentOptional = assignmentRepo.findByAssignmentKey(key);

        if (assignmentOptional.isEmpty()) {
            log.error("Assignment key specified does not exist : {}", key);
            throw new AssignmentExceptions("Assignment not found");
        } else {
            Assignment assignment = assignmentOptional.get();

            //update assignment status based on the deadline
            if(assignment.getAssignmentStatus() == AssignmentStatus.OPEN && assignment.getDeadline().isBefore(LocalDateTime.now())){
                assignment.setAssignmentStatus(AssignmentStatus.CLOSED);
                assignmentRepo.save(assignment);
            }

            return assignmentMapper.toDTO(assignment);
        }
    }

    //get file
    public File getFile(String assignmentKey, String filename) throws MalformedURLException {
        Optional<Assignment> assignmentOptional = assignmentRepo.findByAssignmentKey(assignmentKey);

        if (assignmentOptional.isEmpty()){
            log.error("Could not find assignment with the specified key : {}", assignmentKey);
            try {
                throw new AssignmentExceptions("Assignment Key Not found");
            } catch (AssignmentExceptions e) {
                throw new RuntimeException(e);
            }

        }
        else{
            Assignment assignment = assignmentOptional.get();

            //get assignment folder
            String assignmentFolder = assignment.getFolderName();

            String mediaFolder = applicationProperties.getFolderName();

            String mediaStore = mediaFolder + "/" + assignmentFolder;

            String file = mediaStore + "/" + filename;

            return new File(file);

        }
    }


    public byte[] getFile(File filename){
        try {
            BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream(filename));
            return StreamUtils.copyToByteArray(inputStream);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String getMimeType(File file, String url) throws IOException {
        URLConnection urlConnection = file.toURL().openConnection();

        String mimeType = urlConnection.getContentType();

        if(Objects.equals(mimeType, "content/unknown") && urlConnection != null && url.contains("png")){
            mimeType = "image/png";
        }
        if(Objects.equals(mimeType, "content/unknown") && urlConnection != null && url.contains("jpeg")){
            mimeType = "image/jpeg";
        }
        if(Objects.equals(mimeType, "content/unknown") && urlConnection != null && url.contains("jpg")){
            mimeType = "image/jpg";
        }

        if(Objects.equals(mimeType, "content/unknown") && urlConnection != null && url.contains("jpeg")){
            mimeType = "image/jpeg";
        }
//
//        if(Objects.equals(mimeType, "content/unknown") && urlConnection != null && url.contains("docx")){
//            mimeType = "application/msword";
//        }

        if(Objects.equals(mimeType, "content/unknown") && urlConnection != null && url.contains("docx")){
            mimeType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
        }

        if(Objects.equals(mimeType, "content/unknown") && urlConnection != null && url.contains("pdf")){
            mimeType = "application/pdf";
        }

        if(Objects.equals(mimeType, "content/unknown") && urlConnection != null && url.contains("ppt")){
            mimeType = "application/vnd.ms-powerpoint";
        }

        if(Objects.equals(mimeType, "content/unknown") && urlConnection != null && url.contains("pptx")){
            mimeType = "application/vnd.openxmlformats-officedocument.presentationml.presentation";
        }

        if(Objects.equals(mimeType, "content/unknown") && urlConnection != null && url.contains("txt")){
            mimeType = "text/plain";
        }

        if(Objects.equals(mimeType, "content/unknown") && urlConnection != null && url.contains("xlsx")){
            mimeType ="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        }

        if(Objects.equals(mimeType, "content/unknown") && urlConnection != null && url.contains("odt")){
            mimeType ="application/vnd.oasis.opendocument.text";
        }

        if(Objects.equals(mimeType, "content/unknown") && urlConnection != null && url.contains("zip")){
            mimeType ="application/zip";
        }

    return mimeType;

    }
}
