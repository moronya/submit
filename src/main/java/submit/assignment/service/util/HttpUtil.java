package submit.assignment.service.util;

import lombok.extern.slf4j.Slf4j;
import okhttp3.*;


import java.io.IOException;
import java.util.Map;

@Slf4j
public class HttpUtil {
    public static String post(String url, String body, Map<String, String> headerMap, MediaType mediaType) throws IOException {
        String name = null;
        String value = null;

        for (Map.Entry<String, String> pair : headerMap.entrySet()) {
            name = pair.getKey();

            value = pair.getValue();
        }

        OkHttpClient client = new OkHttpClient();

        RequestBody requestBody = RequestBody.create(body, mediaType);

        Request request = new Request.Builder()
                .url(url)
                .addHeader(name, value)
                .post(requestBody)
                .build();

        log.info("Headers {} ", request.headers());
        log.info("Request {} ", request);

        try (
                Response response = client.newCall(request).execute();

        ) {
            return response.body().string();
        }
    }
}
