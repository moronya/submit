package submit.assignment.service.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class StringUtil {
    public static String combineBy(List values, String delimiter) {
        StringBuilder value = new StringBuilder();

        if (values != null && !values.isEmpty()) {
            for (int i = 0, valuesSize = values.size(); i < valuesSize; i++) {
                value.append(values.get(i));
                if (i < valuesSize - 1) {
                    value.append(delimiter);
                }
            }
        }

        return value.toString();
    }

    public static List<String> splitString(String value, String delimiter){
        List<String> list = new ArrayList<>();
        if(value!=null && !value.trim().isEmpty() && delimiter!=null && !delimiter.trim().isEmpty()){
            list = Arrays.asList(value.split(delimiter));
            list.removeIf(s->s==null||s.trim().isEmpty());
        }
        return new LinkedList<>(list);
    }
}
