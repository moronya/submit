package submit.assignment.service.util;

import org.apache.commons.lang3.RandomStringUtils;

public class RandomUtil {
    private static int key = 6;

    private static int courseKey = 20;

    private static int assignmentKey = 20;

    private static int enrollmentKey = 7;

    private static int folderId = 20;

    public static Long generateResetkey(){
        return Long.valueOf(RandomStringUtils.randomNumeric(key));
    }

    public static String generateCourseEnrollmentKey(){
        return RandomStringUtils.randomAlphanumeric(courseKey);

    }

    public static String generateAssignmentKey(){
        return RandomStringUtils.randomAlphanumeric(assignmentKey);

    }

    public static Long generateEnrollmentKey(){
        return Long.valueOf(RandomStringUtils.randomNumeric(enrollmentKey));
    }

    //generate random folder-name
    public static String generateFolderName(){
        return RandomStringUtils.randomAlphanumeric(folderId);
    }
}
