package submit.assignment.service.util;

public class TemplateUtil {
    public static String activateAccountEmail(Long activationCode)
    {
        return "<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "\n" +
                "<head>\n" +
                "  <meta charset=\"UTF-8\">\n" +
                "  <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n" +
                "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" +
                "  <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\"\n" +
                "    integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" crossorigin=\"anonymous\">\n" +
                "  <title>Submit Account Activation</title>\n" +
                "</head>\n" +
                "\n" +
                "<body>\n" +
                "<p>Hi, use the code below to complete the setup for your Account. " +
                "</p>\n"+
                "<p>"+activationCode+"</p>\n"+
                "<p><b>Thank you for using our service.</b></p>"+
                "</body>\n" +
                "\n" +
                "</html>";
    }


    public static String resetPassword(Long activationCode)
    {
        return "<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "\n" +
                "<head>\n" +
                "  <meta charset=\"UTF-8\">\n" +
                "  <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n" +
                "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" +
                "  <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\"\n" +
                "    integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" crossorigin=\"anonymous\">\n" +
                "  <title>Submit Account Activation</title>\n" +
                "</head>\n" +
                "\n" +
                "<body>\n" +
                "<p>Hi, use the code below to reset your password. " +
                "</p>\n"+
                "<p>"+activationCode+"</p>\n"+
                "<p><b>Thank you for using our service.</b></p>"+
                "</body>\n" +
                "\n" +
                "</html>";
    }
}
