package submit.assignment.service.dto;

import lombok.Data;


@Data
public class SubmissionDTO {
    private String email;

    private String assignmentKey;

    private String studentName;

    private String admissionNumber;
}
