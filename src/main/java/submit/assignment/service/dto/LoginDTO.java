package submit.assignment.service.dto;

import lombok.Data;

@Data
public class LoginDTO {

    private String email;

    private String password;
}
