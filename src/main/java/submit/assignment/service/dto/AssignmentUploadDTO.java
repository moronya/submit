package submit.assignment.service.dto;

import lombok.Data;
import submit.assignment.domain.UploadStatus;

@Data
public class AssignmentUploadDTO {
    private String contentType;

    private String fileName;

    private UploadStatus uploadStatus;

    private String description;

    private String originalFileName;

    private String name;

    private Long size;

    private String folder;
}
