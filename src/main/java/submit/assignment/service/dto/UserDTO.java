package submit.assignment.service.dto;

import lombok.Data;
import submit.assignment.domain.Role;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class UserDTO {
    private Long id;

    private String email;

    private String password;

    private Role role;

    private Long resetKey;

    private LocalDateTime registrationDate;

    private LocalDateTime updatedOn;

    private boolean activated;

    private List<String> courseIds;
}
