package submit.assignment.service.dto;

import lombok.Data;

@Data
public class RequestPasswordReset {
    private String email;
}
