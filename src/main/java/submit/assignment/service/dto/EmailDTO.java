package submit.assignment.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class EmailDTO {
    private String receiverEmail;

    private String subject;

    private String body;

    private boolean isHtml;

    private boolean isMultipart;
}
