package submit.assignment.service.dto;

import lombok.Data;

@Data
public class CourseIdsDTO {
    private Long courseId;
}
