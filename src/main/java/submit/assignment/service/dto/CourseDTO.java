package submit.assignment.service.dto;

import lombok.Data;

import javax.persistence.Column;
import java.util.List;

@Data
public class CourseDTO {

    private Long id;

    private String courseCode;

    private String courseName;

    private String courseInstructor;

    private String enrollmentLink;

    private Long enrollmentKey;

    private String courseDesciption;

    private List<String> enrolledStudents;
}
