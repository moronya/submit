package submit.assignment.service.dto;

import lombok.Data;

@Data
public class EnrollCourseDTO {
    private String email;

    private Long enrollmentKey;
}
