package submit.assignment.service.dto;

import lombok.Data;

@Data
public class ResetPasswordDTO {
    private Long resetKey;

    private String newPassword;
}
