package submit.assignment.service.dto;

import lombok.Data;
import submit.assignment.domain.AssignmentStatus;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class AssignmentDTO {

    private Long id;

    private Long courseId;

    private String title;

    private String instructions;

    private List<String> fileUrls;

    private LocalDateTime createdOn;

    private LocalDateTime deadline;

    private String folderName;

    private String assignmentKey;

    private AssignmentStatus assignmentStatus;

}
