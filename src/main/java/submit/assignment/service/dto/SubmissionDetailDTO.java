package submit.assignment.service.dto;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class SubmissionDetailDTO {
    private Long id;

    private String studentName;

    private String admissionNumber;

    private String assignmentKey;

    private List<String> fileUrls;


    private LocalDateTime submittedAt;
}
