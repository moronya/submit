package submit.assignment.service.dto;

import lombok.Data;
import submit.assignment.domain.Role;

@Data
public class InitialRegistrationDTO {
    private Role role;
    private String email;
}
