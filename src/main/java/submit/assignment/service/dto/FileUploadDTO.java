package submit.assignment.service.dto;

import lombok.Data;

@Data
public class FileUploadDTO {
    private String fileName;
    private Long fileSize;
    private String downloadURI;
}
