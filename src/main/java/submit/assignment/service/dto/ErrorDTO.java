package submit.assignment.service.dto;

import lombok.Data;

@Data
public class ErrorDTO {
    private int statusCode;

    private String description;

    private Status status;

    public enum Status{
        FAIL, SUCCESS
    }

    public ErrorDTO() {
    }

    public ErrorDTO(int statusCode, String description) {
        this.statusCode = statusCode;
        this.description = description;
        this.status = Status.FAIL;
    }

}
