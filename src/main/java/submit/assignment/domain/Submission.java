package submit.assignment.domain;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "tbl_submissions")
public class Submission {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String studentName;

    private String admissionNumber;

    private String assignmentKey;

    private String fileUrls;

    private LocalDateTime submittedAt;

    private String email;
}
