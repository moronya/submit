package submit.assignment.domain;

import lombok.Data;

import javax.persistence.*;

@Data
@Table(name = "tbl_courses")
@Entity
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String courseCode;

    private String courseName;

    @Column(unique = true)
    private String enrollmentLink;

    private String courseInstructor; //course instructor

    @Column(unique = true)
    private Long enrollmentKey; //dynamically generated enrollment key


    @Column(length = 2000)
    private String enrolledStudents;

    private String courseDescription;


}
