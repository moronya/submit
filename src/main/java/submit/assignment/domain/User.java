package submit.assignment.domain;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Entity
@Table(name = "tbl_user_accounts")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    private String email;


    private String password;

    @Enumerated(EnumType.STRING)
    private Role role;

    private Long resetKey;

    private LocalDateTime registrationDate;

    private LocalDateTime updatedOn;

    private boolean activated;

    private String courseIds; //comma separated course ids


}
