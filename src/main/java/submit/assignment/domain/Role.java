package submit.assignment.domain;

public enum Role {
    INSTRUCTOR, STUDENT
}
