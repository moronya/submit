package submit.assignment.domain;

public enum AssignmentStatus {
    OPEN, CLOSED
}
