package submit.assignment.domain;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@Table (name = "tbl_assignments")
public class Assignment {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private Long courseId;

    @Column(length = 65555)
    private String title;

    @Column(length = 65555)
    private String instructions;

    private String fileUrls;

    private LocalDateTime createdOn;

    @Column(unique = true, nullable = false)
    private String folderName;

    private LocalDateTime deadline;

    @Column(unique = true, nullable = false)
    private String assignmentKey;

    @Enumerated(EnumType.STRING)
    private AssignmentStatus assignmentStatus;

}
