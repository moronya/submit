package submit.assignment.web.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import submit.assignment.domain.Course;
import submit.assignment.service.CourseService;
import submit.assignment.service.dto.CourseDTO;
import submit.assignment.service.dto.EnrollCourseDTO;
import submit.assignment.service.dto.ErrorDTO;
import submit.assignment.service.exceptions.CourseNotFoundException;
import submit.assignment.service.exceptions.EmailDoesNotExistException;
import submit.assignment.service.vm.CourseVM;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api")
public class CourseResource {
    private final CourseService courseService;

    public CourseResource(CourseService courseService) {
        this.courseService = courseService;
    }

    //create course
    @PostMapping("/course/create/{email}")
    public Course createCourse (@PathVariable String email, @RequestBody CourseVM courseVM) throws EmailDoesNotExistException {
        log.info("Rest request by instructor : {} to create a course", email);
        courseVM.setInstructor(email);
        Course course = courseService.createCourse(courseVM);
        log.info("Created course : {}", course);
        return course;
    }

    //  enroll to a course
    @PostMapping ("/course/enroll/{email}")
    public ResponseEntity<?> enrollCourse(@PathVariable String email, @RequestBody EnrollCourseDTO enrollCourseDTO) {
        enrollCourseDTO.setEmail(email);
        log.info("REST request for {} to enroll to a course {}", email,
                enrollCourseDTO.getEnrollmentKey());
        try {
            courseService.enrollCourse(enrollCourseDTO);
        } catch (CourseNotFoundException e) {

            ErrorDTO errorDTO = new ErrorDTO(400, e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorDTO.getDescription());
        }
        return ResponseEntity.ok().body("Enrolled to course successfully");


    }

//    fetch courses by user token
    @GetMapping("/courses/{email}")
    public List<Course> fetchCourses(@PathVariable String email){
        log.info("REST request to fetch course");
        return courseService.fetchCourses(email);
    }


}
