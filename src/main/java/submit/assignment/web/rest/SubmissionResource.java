package submit.assignment.web.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import submit.assignment.domain.Submission;
import submit.assignment.service.SubmissionService;
import submit.assignment.service.dto.ErrorDTO;
import submit.assignment.service.dto.SubmissionDTO;
import submit.assignment.service.dto.SubmissionDetailDTO;
import submit.assignment.service.exceptions.AssignmentExceptions;
import submit.assignment.service.exceptions.EmailDoesNotExistException;

import java.io.IOException;
import java.util.List;

@RestController
@Slf4j
@RequestMapping("/api")
public class SubmissionResource {

    private final SubmissionService submissionService;

    private final ObjectMapper objectMapper = new ObjectMapper().findAndRegisterModules();



    public SubmissionResource(SubmissionService submissionService) {
        this.submissionService = submissionService;
    }

    @PostMapping("/submission/create")
    public ResponseEntity<?> createSubmission(@RequestParam String submission,  @RequestParam List<MultipartFile> multipartFileList) throws JsonProcessingException {
        log.info("Request to create submission : {}", submission);

        SubmissionDTO submissionDTO = objectMapper.readValue(submission, SubmissionDTO.class);
        try {
            submissionService.createSubmission(submissionDTO, multipartFileList);
        } catch (EmailDoesNotExistException e) {
            ErrorDTO errorDTO = new ErrorDTO(400, e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorDTO.getDescription());
        } catch (AssignmentExceptions e) {
            ErrorDTO errorDTO = new ErrorDTO(400, e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorDTO.getDescription());
        } catch (IOException e) {
            ErrorDTO errorDTO = new ErrorDTO(400, e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorDTO.getDescription());
        }

        return ResponseEntity.ok().body("Submission was successful");

    }

    @GetMapping("/submission/{assignmentKey}")
    public List<SubmissionDetailDTO> getAllSubmission(@PathVariable String assignmentKey){
        log.info("Request to fetch all submissions for assignment : {}", assignmentKey);
        return submissionService.getAllSubmissions(assignmentKey);
    }
}
