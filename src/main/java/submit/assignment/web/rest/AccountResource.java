package submit.assignment.web.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import submit.assignment.configurations.ApplicationProperties;
import submit.assignment.security.CustomAuthenticationManager;
import submit.assignment.security.jwt.JWTConfig;
import submit.assignment.security.jwt.TokenProvider;
import submit.assignment.service.UserService;
import submit.assignment.service.dto.LoginDTO;
import submit.assignment.service.exceptions.EmailDoesNotExistException;

@RestController
@RequestMapping("/api")
public class AccountResource {
    private final Logger logger = LoggerFactory.getLogger(AccountResource.class);
    @Autowired
    private final TokenProvider tokenProvider;
    private final CustomAuthenticationManager customAuthenticationManager;
    private final ApplicationProperties applicationProperties;
    private final UserService userService;

    public AccountResource(TokenProvider tokenProvider, CustomAuthenticationManager customAuthenticationManager, ApplicationProperties applicationProperties, UserService userService) {
        this.tokenProvider = tokenProvider;
        this.customAuthenticationManager = customAuthenticationManager;
        this.applicationProperties = applicationProperties;
        this.userService = userService;
    }
    @PostMapping("/auth")
    public ResponseEntity<JWTToken> authorize( @RequestBody LoginDTO loginDTO) throws EmailDoesNotExistException {
        logger.debug("Request to authorize user : {} ",loginDTO.getEmail());
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(loginDTO.getEmail(),loginDTO.getPassword());
        Authentication authentication = this.customAuthenticationManager.authenticate(usernamePasswordAuthenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = tokenProvider.createToken(authentication, applicationProperties.getTokenValidity());
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(JWTConfig.AUTHORIZATION_HEADER,"Bearer "+jwt);
        return new ResponseEntity<>(new JWTToken(jwt),httpHeaders, HttpStatus.OK);
    }

    static class JWTToken{
        private String idToken;

        JWTToken(String idToken){
            this.idToken = idToken;
        }
        @JsonProperty
        String getIdToken(){
            return idToken;
        }

        void setIdToken(String idToken){
            this.idToken = idToken;
        }
    }

}
