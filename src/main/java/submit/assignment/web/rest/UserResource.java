package submit.assignment.web.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import submit.assignment.service.UserService;
import submit.assignment.service.dto.*;
import submit.assignment.service.exceptions.EmailDoesNotExistException;
import submit.assignment.service.exceptions.EmailExistsException;
import submit.assignment.service.exceptions.InvalidResetKeyException;

import java.io.IOException;

@Slf4j
@RestController
@RequestMapping("/api/")
public class UserResource {
    private final UserService userService;
    public UserResource(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/user/register")
    public ResponseEntity<?> initialRegister(@RequestBody InitialRegistrationDTO initialRegistrationDTO) throws IOException {
        log.info("User data: {}",initialRegistrationDTO );
        log.info("REST request to register User : {}", initialRegistrationDTO.getEmail());
        try {
            userService.register(initialRegistrationDTO);
        } catch (EmailExistsException e) {
            ErrorDTO errorDTO = new ErrorDTO(400, e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorDTO.getDescription());
        }
        return ResponseEntity.ok().body("Initial User registration has been completed");
    }

    @PostMapping("/user/register/complete")
    public ResponseEntity<?> completeRegistration(@RequestBody AccountInfoDTO accountInfoDTO) {
        log.info("REST request to complete user registration");
        try {
            userService.completeUserRegistration(accountInfoDTO);
        } catch (InvalidResetKeyException e) {
            ErrorDTO errorDTO = new ErrorDTO(400, e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorDTO.getDescription());
        }
        return ResponseEntity.ok().body("Finished setting up account for the user");
    }

    //request password - reset
    @PostMapping("/request/password/reset")
    public ResponseEntity<?> requestPasswordReset(@RequestBody RequestPasswordReset requestPasswordReset) throws IOException {
        try {
            userService.requestPasswordReset(requestPasswordReset);
        } catch (EmailDoesNotExistException e) {
            ErrorDTO errorDTO = new ErrorDTO(400, e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorDTO);

        }
        return ResponseEntity.ok().body("Request password changed for the user completed successfully");
    }

    //reset password
    @PutMapping("/password/reset")
    public ResponseEntity<?> resetPassword(@RequestBody ResetPasswordDTO resetPasswordDTO) {
        log.info("Request to reset password");
        try {
            userService.resetPassword(resetPasswordDTO);
        } catch (InvalidResetKeyException e) {
            ErrorDTO errorDTO = new ErrorDTO(400, e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorDTO);
        }
        return ResponseEntity.ok().body("Successfully reset the password");
    }
}
