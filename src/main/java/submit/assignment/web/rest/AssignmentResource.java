package submit.assignment.web.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.*;

import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import submit.assignment.configurations.ApplicationProperties;
import submit.assignment.domain.Assignment;

import submit.assignment.repository.AssignmentRepo;
import submit.assignment.service.AssignmentService;
import submit.assignment.service.dto.AssignmentDTO;
import submit.assignment.service.dto.FileUploadDTO;
import submit.assignment.service.exceptions.AssignmentExceptions;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
@Slf4j
public class AssignmentResource {
    private final AssignmentService assignmentService;

    private final AssignmentRepo assignmentRepo;

    private final ApplicationProperties applicationProperties;


    private final ObjectMapper objectMapper = new ObjectMapper().findAndRegisterModules();

    public AssignmentResource(AssignmentService assignmentService, AssignmentRepo assignmentRepo, ApplicationProperties applicationProperties) {
        this.assignmentService = assignmentService;


        this.assignmentRepo = assignmentRepo;
        this.applicationProperties = applicationProperties;
    }

    @PostMapping("/assignments/create")
    public AssignmentDTO createAssignment(@RequestParam String assignment, @RequestParam(required = false) List<MultipartFile> files) throws Exception {
        log.info("Request create an assignment {} and files {}", assignment, files);

        AssignmentDTO assignmentDTO = objectMapper.readValue(assignment, AssignmentDTO.class);
       return assignmentService.createAssignment(assignmentDTO, files);
    }

    @PostMapping("/uploadFile")
    public ResponseEntity<FileUploadDTO> saveFile(@RequestParam ("file") String file, MultipartFile multipartFile) throws IOException {
        String filename = StringUtils.cleanPath( multipartFile.getOriginalFilename());
        Long size = multipartFile.getSize();

        String fileKey = AssignmentService.saveFile(file, multipartFile);

        FileUploadDTO fileUploadDTO = new FileUploadDTO();

        fileUploadDTO.setFileName(filename);
        fileUploadDTO.setFileSize(size);
        fileUploadDTO.setDownloadURI("/downloadFile/"+fileKey);

        return new ResponseEntity<>(fileUploadDTO, HttpStatus.OK);
    }


    @GetMapping("/assignments/{courseId}")
    public List<Assignment> getAllAssignment(@PathVariable Long courseId){
    log.info("REST request to fetch all assignments for course : {}", courseId);
    return assignmentService.getAllAssignments(courseId);
    }

    //view an assignment
    @GetMapping("/assignment")
    public AssignmentDTO getOne(@RequestParam String assignmentKey) throws AssignmentExceptions {
        log.info("Request to view assignment : {}", assignmentKey);
        return assignmentService.getOne(assignmentKey);
    }


    //get file

    public ResponseEntity<byte[]> getFile(@PathVariable String assignmentKey, @PathVariable String filename){

        HttpHeaders headers = new HttpHeaders();
        log.info("Request to fetch file : {} for assignment : {}", filename, assignmentKey);
        try {
                 File file = assignmentService.getFile(assignmentKey, filename);

                  byte[] media = assignmentService.getFile(file);

                  String mimeType = assignmentService.getMimeType(file, filename);

                 log.info("Retrieved MIMEType : {}", mimeType);

                 headers.setContentType(MediaType.valueOf(mimeType));
                 headers.setContentLength(file.length());
                 headers.set(HttpHeaders.CONTENT_DISPOSITION,  "attachment; filename=" +filename);

                  return new ResponseEntity<>(media, headers, HttpStatus.OK);
        } catch (IOException e) {
           return new ResponseEntity<>(null, headers, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/files/{assignmentKey}/{filename}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String assignmentKey, @PathVariable String filename){
        //get assignment
        Optional<Assignment> assignmentOptional = assignmentRepo.findByAssignmentKey(assignmentKey);
        Assignment assignment = assignmentOptional.get();

        String folder = applicationProperties.getFolderName();
        String assignmentFolder = assignment.getFolderName();
        String mediaStore = folder +"/"+assignmentFolder +"/"+filename;

        log.info("Media to be downloaded: {}", mediaStore);
        FileSystemResource resource = new FileSystemResource(mediaStore);

        log.info("Resource : {} ", resource);
        MediaType mediaType = MediaTypeFactory
                .getMediaType(resource)
                .orElse(MediaType.APPLICATION_OCTET_STREAM);

        log.info("MediaType : {}", mediaType);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(mediaType);

        ContentDisposition disposition = ContentDisposition
                .attachment()
                .filename(resource.getFilename())
                .build();
        headers.setContentDisposition(disposition);

        log.info("Content disposition : {}", disposition);

//        return resource;

        return new ResponseEntity<>(resource, headers, HttpStatus.OK);

    }


}
