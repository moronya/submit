package submit.assignment.security.jwt;

import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;
import submit.assignment.configurations.ApplicationProperties;
import submit.assignment.service.UserService;

import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

import io.jsonwebtoken.security.Keys;



@Slf4j
@Component
public class TokenProvider {
    public final ApplicationProperties applicationProperties;

    private final UserService userService;

    private String jwtSecret;

    private long tokenValidityInMilliSeconds;

    private static final String AUTHORITY_KEY = "auth";

    private final Key key;

    private final JwtParser jwtParser;


    public TokenProvider(ApplicationProperties applicationProperties, UserService userService) {
        this.applicationProperties = applicationProperties;
        this.userService = userService;
        this.jwtSecret = applicationProperties.getJwtSecret();
        this.tokenValidityInMilliSeconds = applicationProperties.getTokenValidity();
        key = Keys.hmacShaKeyFor(jwtSecret.getBytes(StandardCharsets.UTF_8));
        jwtParser = Jwts.parserBuilder().setSigningKey(key).build();

    }

    public String createToken(Authentication authentication, long tokenValidity){
        //list of authorities
        String authorities = authentication.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.joining());

        CustomAuthentication customAuthentication = (CustomAuthentication) authentication;

        Long now = new Date().getTime();

        Date validity;

        validity = new Date(now + tokenValidityInMilliSeconds);

        JwtBuilder builder = Jwts.builder().setSubject(customAuthentication.getName()).claim(AUTHORITY_KEY, authorities);

        userService.getUserWithRolesByLogin(customAuthentication.getName());

    return builder.signWith(SignatureAlgorithm.HS512, key).setExpiration(validity).compact();
    }

    public boolean validateToken(String authToken){
        try{
            jwtParser.parseClaimsJws(authToken);
            return true;
        }catch(MalformedJwtException e){
            log.info("Invalid JWT token");
        }catch(ExpiredJwtException e){
            log.info("Expired JWT exception");
        }catch(UnsupportedJwtException e){
            log.info("Unsupported JWT exception");
        }catch (IllegalArgumentException e){
            log.info("JWT token compact of handler are invalid");
        }
        return false;
    }

    public Authentication getAuthentication(String token){
        Claims claims = jwtParser.parseClaimsJws(token).getBody();

        Collection<? extends GrantedAuthority> authorities = Arrays.
                stream(claims.get(AUTHORITY_KEY).toString().split(","))
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());

        User principal = new User(claims.getSubject(), "", authorities);
        CustomAuthentication customAuthentication = new CustomAuthentication(principal, token ,authorities);
        return customAuthentication;


    }


}
