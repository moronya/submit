package submit.assignment.security;

import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import submit.assignment.domain.Role;
import submit.assignment.domain.User;
import submit.assignment.security.jwt.CustomAuthentication;
import submit.assignment.service.UserService;
import submit.assignment.service.exceptions.EmailDoesNotExistException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class CustomAuthenticationManager {
    private final UserService userService;
    private final PasswordEncoder passwordEncoder;

    public CustomAuthenticationManager(UserService userService, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
    }

    public Authentication authenticate(Authentication authentication) throws EmailDoesNotExistException {
        //get principal Email and Password
        String email = authentication.getPrincipal().toString();

        String password = authentication.getCredentials().toString();

        //check whether the user with the provided email exists
        Optional<User> optionalUser = userService.getUserWithRolesByLogin(email);

        if(!optionalUser.isPresent()){
            throw new EmailDoesNotExistException("User not found");
        }
        //email exists, therefore, get the user
        User user = optionalUser.get();

        if(!password.isEmpty()){
            if (!passwordEncoder.matches(password, user.getPassword())) {
                throw new BadCredentialsException("Invalid Credentials");
            }
        }else{
            throw new BadCredentialsException("Invalid Credentials");
        }

        Role userRole = user.getRole();
        List<GrantedAuthority> grantedAuthorityList = new ArrayList<>();

        //set the default authority
        GrantedAuthority grantedAuthority = new CustomAuthority(Role.STUDENT.name());

        if(userRole == Role.INSTRUCTOR){
            grantedAuthority = new CustomAuthority(Role.INSTRUCTOR.name());
        }
        else if(userRole == Role.STUDENT){
            grantedAuthority = new CustomAuthority(Role.STUDENT.name());
        }
        grantedAuthorityList.add(grantedAuthority);

        return new CustomAuthentication(email, null, grantedAuthorityList);

    }
}
