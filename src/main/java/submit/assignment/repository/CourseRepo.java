package submit.assignment.repository;

import org.springframework.data.repository.CrudRepository;
import submit.assignment.domain.Course;
import submit.assignment.service.dto.CourseIdsDTO;

import java.util.List;
import java.util.Optional;

public interface CourseRepo extends CrudRepository<Course, Long> {
        Optional<Course> findByEnrollmentKey(Long key);
        Optional<Course> findById(Long id);


}
