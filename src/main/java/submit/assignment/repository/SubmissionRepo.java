package submit.assignment.repository;

import org.springframework.data.repository.CrudRepository;
import submit.assignment.domain.Submission;
import submit.assignment.service.dto.SubmissionDetailDTO;

public interface SubmissionRepo extends CrudRepository<Submission, Long> {
    Iterable<Submission> findByAssignmentKey(String assignmentKey);
}
