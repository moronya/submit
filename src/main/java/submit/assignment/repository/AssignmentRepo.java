package submit.assignment.repository;

import org.springframework.data.repository.CrudRepository;
import submit.assignment.domain.Assignment;

import java.util.Optional;

public interface AssignmentRepo extends CrudRepository<Assignment, Long> {
    Iterable<Assignment> findByCourseId(Long id);
    Optional<Assignment> findByAssignmentKey(String key);
}
