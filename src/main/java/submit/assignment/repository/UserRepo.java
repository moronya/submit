package submit.assignment.repository;

import org.springframework.data.repository.CrudRepository;
import submit.assignment.domain.Course;
import submit.assignment.domain.User;

import java.util.Optional;

public interface UserRepo extends CrudRepository<User, Long> {
    Optional<User> findByEmailIgnoreCase(String email);
    Optional<User> findOneWithRoleByEmail(String login);
    Optional<User> findByResetKey(Long code);

  

}
