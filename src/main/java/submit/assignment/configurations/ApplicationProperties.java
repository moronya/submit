package submit.assignment.configurations;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = true)
public class ApplicationProperties {
    private String apiKey;

    private String notify;

    private String jwtSecret;

    private Long tokenValidity;

    private String folderName;
}
